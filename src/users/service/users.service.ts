import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserReqDTO } from '../model/dtos/request/user.request.dto';
import { UserEntity } from '../model/entities/user.entity';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepo: Repository<UserEntity>,
  ) {}

  async createUser(userDTO: UserReqDTO) {
    const newUser = this.userRepo.create(userDTO);
    newUser.password = await bcrypt.hash(userDTO.password, 10);
    return await this.userRepo.save(newUser);
  }

  findUserByUsername(username: string): Promise<UserEntity> {
    return this.userRepo.findOne({
      where: {
        username: username,
      },
    });
  }

  findUserById(id: number): Promise<UserEntity> {
    return this.userRepo.findOne({
      where: {
        id: id,
      },
    });
  }

  async updateUser(userId: number, data: any): Promise<UserEntity> {
    const user = await this.findUserById(userId);
    if (user) {
      user.email = data.email ?? user.email;
      user.username = data.username ?? user.username;
      user.password = data.password
        ? await bcrypt.hash(data.password, 10)
        : user.password;
      user.firstName = data.firstName ?? user.firstName;
      user.lastName = data.lastName ?? user.lastName;
      user.phone = data.phone ?? user.phone;
      return this.userRepo.save(user);
    } else {
      return null;
    }
  }

  async validateSignIn(username: string, password: string) {
    const userDB = await this.findUserByUsername(username);
    if (!userDB) {
      return null;
    } else {
      const matched = bcrypt.compareSync(password, userDB.password);
      return matched ? userDB : null;
    }
  }
}
