import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
  Req,
  UseFilters,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { BaseResponse } from 'src/common/base_response.model';
import { HttpExceptionFilter } from 'src/filters/http_exception_filter';
import { ChangePasswordReqDTO } from '../model/dtos/request/change_password.request.dto';
import { UpdateUserReqDto } from '../model/dtos/request/update_user.request.dto';
import { UserResDTO } from '../model/dtos/response/user.response.dto';
import { UsersService } from '../service/users.service';

@Controller('users')
export class UsersController {
  constructor(private readonly userService: UsersService) {}

  @Get('me')
  @UseGuards(AuthGuard('jwt'))
  @UseFilters(HttpExceptionFilter)
  @HttpCode(HttpStatus.OK)
  async getCurrentUser(@Req() req) {
    const user = await this.userService.findUserByUsername(req.user.username);
    if (user) {
      return new BaseResponse(
        'Get info user successfully',
        new UserResDTO(user),
      );
    } else {
      throw new HttpException('User not found', HttpStatus.NOT_FOUND);
    }
  }

  @Post('change-password')
  @UseGuards(AuthGuard('jwt'))
  @UsePipes(ValidationPipe)
  @UseFilters(HttpExceptionFilter)
  @HttpCode(HttpStatus.OK)
  async updatePassword(@Req() req, @Body() body: ChangePasswordReqDTO) {
    if (body.newPassword === body.oldPassword) {
      throw new HttpException(
        'newPassword is oldPassword',
        HttpStatus.BAD_REQUEST,
      );
    }
    const user = await this.userService.validateSignIn(
      req.user.username,
      body.oldPassword,
    );
    if (user) {
      const userNewPass = await this.userService.updateUser(user.id, {
        password: body.newPassword,
      });
      if (userNewPass) {
        return new BaseResponse('Update password success', {});
      } else {
        throw new HttpException(
          'Internal server error',
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    } else {
      throw new HttpException(
        'Old password is not correct',
        HttpStatus.NOT_FOUND,
      );
    }
  }

  @Put(':id')
  @UseGuards(AuthGuard('jwt'))
  @UsePipes(ValidationPipe)
  @UseFilters(HttpExceptionFilter)
  @HttpCode(HttpStatus.OK)
  async updateUser(@Req() req, @Param() param, @Body() body: UpdateUserReqDto) {
    const user = await this.userService.findUserById(param.id);
    if (user) {
      if (user.username !== req.user.username) {
        throw new HttpException('Cannot update', HttpStatus.NOT_FOUND);
      }
      const newUser = await this.userService.updateUser(param.id, body);
      if (newUser) {
        return new BaseResponse('Update user success', new UserResDTO(newUser));
      } else {
        throw new HttpException(
          'Internal server error',
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    } else {
      throw new HttpException('User not found', HttpStatus.NOT_FOUND);
    }
  }
}
