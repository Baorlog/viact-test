import { Exclude } from "class-transformer";
import { IsNotEmpty, IsEmail } from "class-validator";
import { UserEntity } from "../../entities/user.entity";

export class UserResDTO {
    constructor(user?: UserEntity) {
        this.id = user?.id;
        this.username = user?.username;
        this.password = user?.password;
        this.email = user?.email;
        this.firstName = user?.firstName;
        this.lastName = user?.lastName;
        this.phone = user?.phone;
      }
    
      id: number;
    
      @IsNotEmpty()
      username: string;
    
      @IsNotEmpty()
      @Exclude()
      password: string;
    
      @IsEmail()
      email: string;
    
      firstName: string;
    
      lastName: string;
    
      phone: string;
}