import { IsEmail, IsNotEmpty, IsPhoneNumber } from 'class-validator';

export class UserReqDTO {
  id: number;

  @IsNotEmpty()
  username: string;

  @IsNotEmpty()
  password: string;

  @IsEmail()
  email: string;

  firstName: string;

  lastName: string;

  @IsPhoneNumber('VN')
  phone: string;
}
