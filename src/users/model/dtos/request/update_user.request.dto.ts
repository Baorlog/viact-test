import { IsPhoneNumber } from 'class-validator';

export class UpdateUserReqDto {
  email: string;

  firstName: string;

  lastName: string;

  @IsPhoneNumber('VN')
  phone: string;
}
