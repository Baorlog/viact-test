import { IsNotEmpty } from 'class-validator';

export class ChangePasswordReqDTO {
  @IsNotEmpty()
  oldPassword: string;

  @IsNotEmpty()
  newPassword: string;
}
