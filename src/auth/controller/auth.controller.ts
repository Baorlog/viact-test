import {
  Body,
  Controller,
  HttpCode,
  HttpException,
  HttpStatus,
  InternalServerErrorException,
  Post,
  Req,
  UseFilters,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { HttpExceptionFilter } from 'src/filters/http_exception_filter';
import { UserReqDTO } from 'src/users/model/dtos/request/user.request.dto';
import { UsersService } from 'src/users/service/users.service';
import { AuthService } from '../service/auth.service';
import { TokenService } from '../service/token.service';
import { BaseResponse } from 'src/common/base_response.model';
import { AuthGuard } from '@nestjs/passport';
import { ReauthReqDTO } from '../model/dtos/reauth.request.dto';

@Controller('auth')
export class AuthController {
  constructor(
    private readonly userService: UsersService,
    private readonly authService: AuthService,
    private readonly tokenService: TokenService,
  ) {}

  @Post('sign-up')
  @UsePipes(ValidationPipe)
  @UseFilters(HttpExceptionFilter)
  @HttpCode(HttpStatus.OK)
  async signUp(@Body() userDTO: UserReqDTO) {
    const user = await this.userService.findUserByUsername(userDTO.username);
    if (!user) {
      const result = this.userService.createUser(userDTO);
      if (result) {
        return new BaseResponse('Sign up Success', {});
      } else {
        throw new InternalServerErrorException();
      }
    } else {
      throw new HttpException('User already exists', HttpStatus.BAD_REQUEST);
    }
  }

  @Post('sign-in')
  @UseGuards(AuthGuard('local'))
  @UsePipes(ValidationPipe)
  @UseFilters(HttpExceptionFilter)
  @HttpCode(HttpStatus.OK)
  async signIn(@Req() req) {
    const user = await this.userService.findUserByUsername(req.user.username);
    if (user) {
      const accessToken = this.tokenService.signJwt(
        { id: user.id, username: user.username },
        process.env.ACCESS_TOKEN_PRIVATE_KEY,
        {
          expiresIn: process.env.JWT_EXPIRATION,
        },
      );
      const refreshToken = this.tokenService.signJwt(
        { id: user.id, username: user.username },
        process.env.REFRESH_PRIVATE_KEY,
        {
          expiresIn: process.env.REFRESH_EXPIRATION,
        },
      );
      return new BaseResponse('Login successfully', {
        accessToken,
        refreshToken,
      });
    } else {
      throw new HttpException('User not found', HttpStatus.NOT_FOUND);
    }
  }

  @Post('reauth')
  @UsePipes(ValidationPipe)
  @UseFilters(HttpExceptionFilter)
  @HttpCode(HttpStatus.OK)
  async reauth(@Body() body: ReauthReqDTO) {
    const refreshToken = body.refreshToken;
    const username = body.username;
    const decoded = this.tokenService.verifyJwt<{
      id: number;
      username: string;
    }>(refreshToken, process.env.REFRESH_PUBLIC_KEY);
    if (decoded) {
      if (username === decoded.username) {
        const accessToken = this.tokenService.signJwt(
          { id: decoded.id, username: decoded.username },
          process.env.ACCESS_TOKEN_PRIVATE_KEY,
          {
            expiresIn: process.env.JWT_EXPIRATION,
          },
        );
        return new BaseResponse('Refresh access token successfully', {
          accessToken,
          refreshToken,
        });
      } else {
        throw new HttpException(
          'Refresh token is not your',
          HttpStatus.UNAUTHORIZED,
        );
      }
    } else {
      throw new HttpException(
        'Refresh token is expires',
        HttpStatus.UNAUTHORIZED,
      );
    }
  }
}
