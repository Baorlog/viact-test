import { Injectable } from '@nestjs/common';
import { UsersService } from 'src/users/service/users.service';

@Injectable()
export class AuthService {
  constructor(private readonly userService: UsersService) {}

  async validateSignIn(username: string, password: string) {
    return this.userService.validateSignIn(username, password);
  }
}
