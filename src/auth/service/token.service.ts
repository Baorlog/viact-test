import { Injectable } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';

@Injectable()
export class TokenService {
  signJwt(
    object: Object,
    privateKey: string,
    option?: jwt.SignOptions | undefined,
  ) {
    const signingKey = Buffer.from(privateKey, 'base64').toString('ascii');

    return jwt.sign(object, signingKey, {
      ...(option && option),
      algorithm: 'RS256',
    });
  }

  verifyJwt<T>(token: string, publicKey: string): T | null {
    const publicKeyDecode = Buffer.from(publicKey, 'base64').toString('ascii');
    try {
      const decoded = jwt.verify(token, publicKeyDecode) as T;
      return decoded;
    } catch (e) {
      return null;
    }
  }
}
