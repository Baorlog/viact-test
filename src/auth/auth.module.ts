import { Module } from '@nestjs/common';
import { UsersService } from 'src/users/service/users.service';
import { AuthController } from './controller/auth.controller';
import { LocalStrategy } from './middleware/local.strategy';
import { AuthService } from './service/auth.service';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from 'src/users/model/entities/user.entity';
import { JwtModule, JwtModuleOptions } from '@nestjs/jwt';
import { JwtStrategy } from './middleware/jwt.strategy';
import { TokenService } from './service/token.service';
import { UsersModule } from 'src/users/users.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([UserEntity]),
    PassportModule,
    JwtModule.registerAsync({
      useFactory: () => {
        const signingKey = Buffer.from(
          process.env.ACCESS_TOKEN_PRIVATE_KEY,
          'base64',
        ).toString('ascii');
        const publicKey = Buffer.from(
          process.env.ACCESS_TOKEN_PUBLIC_KEY,
          'base64',
        ).toString('ascii');
        const options: JwtModuleOptions = {
          privateKey: signingKey,
          publicKey: publicKey,
          signOptions: {
            expiresIn: process.env.JWT_EXPIRATION,
            algorithm: 'RS256',
          },
        };
        return options;
      },
    }),
    UsersModule,
  ],
  controllers: [AuthController],
  providers: [LocalStrategy, JwtStrategy, AuthService, TokenService],
  exports: [JwtStrategy],
})
export class AuthModule {}
