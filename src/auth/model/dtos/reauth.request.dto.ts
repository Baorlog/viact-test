import { IsNotEmpty } from 'class-validator';

export class ReauthReqDTO {
  @IsNotEmpty()
  username: string;

  @IsNotEmpty()
  refreshToken: string;
}
