import { IsNotEmpty } from 'class-validator';

export class SignInReqDTO {
  @IsNotEmpty()
  username: string;

  @IsNotEmpty()
  password: string;
}
