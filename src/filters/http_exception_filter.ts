import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
} from '@nestjs/common';
import { Response } from 'express';
import { BaseResponse } from 'src/common/base_response.model';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();
    const status = exception.getStatus();
    const exResponse = exception.getResponse();
    let message = '';
    if (typeof exResponse === 'string') {
      message = exResponse;
    } else if (typeof exResponse === 'object') {
      message = exResponse['message'];
    }
    response.status(status).json(new BaseResponse(message, {}));
  }
}
