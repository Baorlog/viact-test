FROM --platform=linux/amd64 node:18-alpine

WORKDIR /app

COPY . .

RUN npm i -g @nestjs/cli

RUN yarn install

EXPOSE 5001

CMD ["nest", "start"]